﻿using Tetris.Utilities;

namespace Tetris.Entity
{
	class ITetromino : Tetromino
	{
		/// <summary>
		/// - - - - <para/>
		/// - - - - <para/>
		/// x x x x <para/>
		/// - - - -
		/// </summary>
		public ITetromino() : base(false)
		{
			// Initialize the bounding box.
			InitialBoundingBox = new bool[,] {
				{ false, false, false, false },
				{ false, false, false, false },
				{ true,  true,  true,  true  },
				{ false, false, false, false } };
			UpdateBoundingBox(InitialBoundingBox);

			// Since this tetromino only has two rotate states, initialize the other.
			RotatedBoundingBox = MathUtilities.RotateMatrixCounterClockwise(InitialBoundingBox);
		}
	}
}
