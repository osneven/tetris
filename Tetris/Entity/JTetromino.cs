﻿namespace Tetris.Entity
{
	class JTetromino : Tetromino
	{
		/// <summary>
		/// - - -<para/>
		/// x x x<para/>
		/// - - x
		/// </summary>
		public JTetromino() : base()
		{
			InitialBoundingBox = new bool[,] {
				{ false, false, false },
				{ true,  true,  true  },
				{ false, false, true  }
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}

