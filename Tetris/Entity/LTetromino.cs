﻿namespace Tetris.Entity
{
	class LTetromino : Tetromino
	{
		/// <summary>
		/// - - -<para/>
		/// x x x<para/>
		/// x - -
		/// </summary>
		public LTetromino() : base()
		{
			InitialBoundingBox = new bool[,] {
				{ false, false, false },
				{ true,  true,  true  },
				{ true,  false, false }
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}
