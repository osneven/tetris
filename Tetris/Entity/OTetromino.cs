﻿namespace Tetris.Entity
{
	class OTetromino : Tetromino
	{
		/// <summary>
		/// x x<para/>
		/// x x<para/>
		/// </summary>
		public OTetromino() : base(false)
		{
			InitialBoundingBox = new bool[,] {
				{ true,  true},
				{ true,  true}
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}
