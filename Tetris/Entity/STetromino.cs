﻿namespace Tetris.Entity
{
	class STetromino : Tetromino
	{
		/// <summary>
		/// - - -<para/>
		/// - x x<para/>
		/// x x -
		/// </summary>
		public STetromino() : base()
		{
			InitialBoundingBox = new bool[,] {
				{ false, false, false },
				{ false, true,  true  },
				{ true,  true,  false }
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}
