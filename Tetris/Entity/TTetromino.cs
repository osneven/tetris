﻿namespace Tetris.Entity
{
	class TTetromino : Tetromino
	{
		/// <summary>
		/// - - -<para/>
		/// x x x<para/>
		/// - x -
		/// </summary>
		public TTetromino() : base()
		{
			InitialBoundingBox = new bool[,] {
				{ false, false,  false },
				{ true,  true,   true  },
				{ false, true,   false }
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}
