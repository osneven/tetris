﻿using System.Collections.Generic;

namespace Tetris.Entity
{
	/// <summary>
	/// An abstract game piece.
	/// </summary>
	class Tetromino
	{



		/// <summary>
		/// Determines whether this tetromino can be rotated.
		/// </summary>
		public bool CanRotate;

		/// <summary>
		/// The bounding box containing the shape of the tetromino.
		/// </summary>
		public bool[,] BoundingBox { get; set; }

		/// <summary>
		/// The initial state of the bounding box, free of any rotations that might have taken place.
		/// </summary>
		public bool[,] InitialBoundingBox { get; protected set; }

		/// <summary>
		/// The rotated state of the bounding box. Will only be initialized in special cases.
		/// </summary>
		public bool[,] RotatedBoundingBox { get; protected set; }

		/// <summary>
		/// The width of the bounding box.
		/// </summary>
		public int BoundingBoxWidth { get; private set; }

		/// <summary>
		/// The height of the bounding box.
		/// </summary>
		public int BoundingBoxHeight { get; private set; }

		/// <summary>
		/// Initializes the bounding box using the InitializeBoundingBox() method.
		/// </summary>
		/// <param name="canRotate">Whether this tetromino can rotate or not.</param>
		public Tetromino(bool canRotate = true, bool[,] rotatedBoundingBox = null)
		{
			CanRotate = canRotate;
			RotatedBoundingBox = rotatedBoundingBox;
		}

		/// <summary>
		/// Inititalizes a new Tetromino by cloning the date from the given one.
		/// </summary>
		/// <param name="clone">The tetromino to clone.</param>
		public Tetromino(Tetromino clone)
		{
			InitialBoundingBox = clone.InitialBoundingBox;
			UpdateBoundingBox(InitialBoundingBox);
			CanRotate = clone.CanRotate;
			RotatedBoundingBox = clone.RotatedBoundingBox;
		}

		/// <summary>
		/// Returns the width of the actual tetromino shape, and not the bounding box's width.
		/// The width of the tetromino is determined by the index of the last column in the bounding box
		/// which contains a filled cell (a piece of the tetromino).
		/// </summary>
		/// <returns>The actual width of the tetromino.</returns>
		public int GetActualWidth()
		{
			int lastNotEmptyColumn = 0;
			for (int i = 0; i < BoundingBoxHeight; i++)
				for (int j = 0; j < BoundingBoxWidth; j++)
					if (BoundingBox[i, j] && lastNotEmptyColumn < j)
						lastNotEmptyColumn = j;
			return lastNotEmptyColumn;
		}

		/// <summary>
		/// Returns the height of the actual tetromino shape, and not the bounding box's height.
		/// The height of the tetromino is determined by the index of the last row in the bounding box
		/// which contains a filled cell (a piece of the tetromino).
		/// </summary>
		/// <returns>The actual height of the tetromino.</returns>
		public int GetActualHeight()
		{
			int lastNotEmptyRow = 0;
			for (int i = 0; i < BoundingBoxHeight; i++)
				for (int j = 0; j < BoundingBoxWidth; j++)
					if (BoundingBox[i, j] && lastNotEmptyRow < i)
						lastNotEmptyRow = i;
			return lastNotEmptyRow;
		}

		/// <summary>
		/// Returns the index of the first column in the bounding box with a filled cell.
		/// </summary>
		/// <returns>The index.</returns>
		public int GetWidthOffset()
		{
			int x = BoundingBoxWidth - 1;
			for (int i = 0; i < BoundingBoxHeight; i++)
				for (int j = 0; j < BoundingBoxWidth; j++)
					if (j < x && BoundingBox[i, j])
						x = j;
			return x;
		}

		/// <summary>
		/// Returns the index of the first row in the bounding box with a filled cell.
		/// </summary>
		/// <returns>The index.</returns>
		public int GetHeightOffset()
		{
			int y = BoundingBoxHeight - 1;
			for (int i = 0; i < BoundingBoxHeight; i++)
				for (int j = 0; j < BoundingBoxWidth; j++)
					if (i < y && BoundingBox[i, j])
						y = i;
			return y;
		}

		/// <summary>
		/// Finds and returns the positions of all the cells in the bounding box which are filled.
		/// </summary>
		/// <returns>The positions of the filled cells.</returns>
		public int[][] GetFilledCells()
		{
			List<int[]> filledCells = new List<int[]>();
			for (int i = 0; i < BoundingBoxHeight; i++)
				for (int j = 0; j < BoundingBoxWidth; j++)
					if (BoundingBox[i, j])
						filledCells.Add(new int[] { i, j });
			return filledCells.ToArray();
		}

		/// <summary>
		/// Determines if the given position is contained in the bounding box.
		/// </summary>
		/// <param name="x">The position on the x-axis.</param>
		/// <param name="y">The position on the y-axis.</param>
		/// <returns>True if it is contained, false if otherwise.</returns>
		public bool Contains(int x, int y)
		{
			return x >= 0 && y >= 0 && x < BoundingBoxWidth && y < BoundingBoxWidth;
		}

		/// <summary>
		/// Updates the bounding box of the tetromino and all the values dependent on it.
		/// </summary>
		/// <param name="boundingBox">The new bounding box.</param>
		public void UpdateBoundingBox(bool[,] boundingBox)
		{
			BoundingBox = boundingBox;
			BoundingBoxHeight = BoundingBox.GetLength(0);
			BoundingBoxWidth = BoundingBox.GetLength(1);
		}

		public Tetromino Clone()
		{
			return new Tetromino(this);
		}
	}
}
