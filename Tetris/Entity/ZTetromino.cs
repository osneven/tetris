﻿namespace Tetris.Entity
{
	class ZTetromino : Tetromino
	{
		/// <summary>
		/// - - -<para/>
		/// x x -<para/>
		/// - x x
		/// </summary>
		public ZTetromino() : base()
		{
			InitialBoundingBox = new bool[,] {
				{ false, false, false },
				{ true,  true,  false },
				{ false, true,  true  }
			};
			UpdateBoundingBox(InitialBoundingBox);
		}
	}
}
