﻿using System;
using System.Text;
using System.Threading;
using Tetris.Entity;
using Tetris.Utilities;

namespace Tetris
{
	class Game
	{
		private Well well;
		private Tetromino hand, hold, next;
		private int x, y, handWidth, handHeigh, lastY, offsetX;
		private bool switched;

		private const int INITIAL_DROP_SPAN = 700; // Milliseconds
		private int dropSpan;
		private DateTime lastDrop;

		private int score;
		private int combo;

		private bool gameRunning, roundRunning;
		private StringBuilder screen;

		public Game()
		{
			gameRunning = true;
		}

		public void Start()
		{
			screen = new StringBuilder();

			// Start the round.
			StartRound();

			// Start the rendering.
			Thread loopThread = new Thread(DoLoop);
			loopThread.Start();

			// Start listening for input.
			Input();
		}

		/// <summary>
		/// Handles for the game loop.
		/// </summary>
		public void DoLoop()
		{
			// Run until the game is quit.
			while (gameRunning)
			{
				// Run until the round ends.
				while (roundRunning)
				{
					// Handle the current frame.
					DoFrame();

					// Wait for next loop.
					Thread.Sleep(100);
				}
			}
		}

		/// <summary>
		/// Does all the functions needed for each frame.
		/// </summary>
		public void DoFrame()
		{
			// Print the screen.
			string s = GetPrint();

			//Console.Clear();
			//Console.SetCursorPosition(0, 0);
			//Console.WriteLine(GetClearPrint());
			Console.SetCursorPosition(0, 0);
			Console.WriteLine(s);

			// Check if a fall need to occur.
			if (DateTime.Now > lastDrop.AddMilliseconds(dropSpan))
				DropHandOnce();
		}

		/// <summary>
		/// Returns the rendered string of the current frame.
		/// </summary>
		/// <returns>The string to print to the screen.</returns>
		private string GetPrint()
		{
			int nextWidth = next.GetActualWidth(), nextHeigh = next.GetActualHeight();
			string nl = new String(' ', Well.WIDTH * 3) + "\n";
			int prevHeight = screen.ToString().Split('\n').Length;

			// Add the HUD to the screen.
			screen = new StringBuilder($"{nl}  Next{new String(' ', Well.WIDTH * 2 - 9)}Hold\n");
			screen.Append(nl);
			int holdHeight = next.BoundingBoxHeight;
			if (hold is Tetromino)
				holdHeight = hold.BoundingBoxHeight;
			int appendedLines = 0;
			for (int i = 0; i < Math.Max(holdHeight, next.BoundingBoxHeight); i++)
			{
				StringBuilder line = new StringBuilder(" ");

				// Add the current row of the next tetromino.
				int nextOffset = 2 - next.BoundingBoxHeight;
				if (i >= nextOffset)
					for (int j = 0; j < nextWidth + 1; j++)
					{
						string s = " ";
						if (i - nextOffset < nextHeigh + 1 && next.BoundingBox[i - nextOffset, j])
							s = "#";
						line.Append($" {s}");
					}

				// Add the current row of the hold tetromino.
				if (hold is Tetromino)
				{
					int holdOffset = 2 - hold.BoundingBoxHeight;
					if (i >= holdOffset)
					{
						int holdWidth = hold.GetActualWidth(), holdHeigh = hold.GetActualHeight();
						line.Append(new String(' ', Well.WIDTH * 2 - hold.BoundingBoxWidth * 2 - next.BoundingBoxWidth * 2));
						for (int j = 0; j < holdWidth + 1; j++)
						{
							string s = " ";
							if (i - holdOffset < holdHeigh + 1 && hold.BoundingBox[i - holdOffset, j])
								s = "#";
							line.Append($" {s}");
						}
					}
				}
				line.Append(nl);

				// Only append the current row to the screen if it contains a filled cell.
				if (line.ToString().Contains('#'))
				{
					screen.Append(line.ToString());
					appendedLines++;
				}
			}
			// Make sure the space between the HUD and well doesn't change due to smaller tetrominos.
			for (int i = 0; i < 2 - appendedLines; i++)
				screen.Append(nl);

			// Add the well to the screen.
			screen.Append($"{nl}{well.GetPrint(hand, x, y)}");

			// Add the score to the screen.
			screen.Append($"{nl}  Score: {score}");

			// Return the screen to print.
			return screen.ToString();
		}

		/// <summary>
		/// Returns the rendered string of the round end frame.
		/// </summary>
		/// <returns>The string to print to the screen.</returns>
		private string GetEndPrint()
		{
			// Add the game over to the screen.
			screen = new StringBuilder();
			screen.Append($"{new String('\n', Well.HEIGHT / 2 - 3)}       GAME OVER");

			// Add the score to the screen.
			screen.Append($"\n\n       Score: {score}");

			// Add menu options to the screen.
			screen.Append("\n\n       Play again (y) or exit (n)?");

			// Return the screen to print.
			return screen.ToString();
		}

		private string GetClearPrint()
		{
			StringBuilder clear = new StringBuilder();
			for (int i = 0; i < Well.HEIGHT * 3; i++)
			{
				clear.Append(new String(' ', Well.WIDTH * 3) + "\n");
			}
			return clear.ToString();
		}

		/// <summary>
		/// Listens for console key input.
		/// </summary>
		public void Input()
		{
			// Run until the game is quit.
			while (gameRunning)
			{
				// Run until the round is quit.
				while (roundRunning)
				{
					switch (Console.ReadKey().Key)
					{
						// Quit.
						case ConsoleKey.Escape:
							//Environment.Exit(1);
							EndRound();
							break;

						// Move right.
						case ConsoleKey.RightArrow:
							MoveRight();
							break;

						// Move left.
						case ConsoleKey.LeftArrow:
							MoveLeft();
							break;

						// Move down.
						case ConsoleKey.DownArrow:
							DropHandOnce();
							DoFrame();
							break;

						// Drop all the way down.
						case ConsoleKey.Spacebar:
							DropHand();
							break;

						// Rotate left.
						case ConsoleKey.Q:
							RotateLeft();
							break;

						// Rotate right.
						case ConsoleKey.UpArrow:
						case ConsoleKey.E:
							RotateRight();
							break;

						// Hold the hand.
						case ConsoleKey.C:
							HoldHand();
							break;


						// Reset hand (cheat).
						case ConsoleKey.N:
							ResetHand();
							break;

						// Toggle gravity (cheat).
						case ConsoleKey.G:
							if (lastDrop > DateTime.Now.AddYears(1)) // Enable.
								lastDrop = DateTime.Now;
							else
								lastDrop = DateTime.Now.AddYears(2); // Disable.
							break;
					}

					// As almost all actions can change this value, update it here.
					lastY = well.DropTetromino(hand, x, y);
				}

				// If we're here, a round was ended. Handle for new input.
				while (!roundRunning)
				{
					switch (Console.ReadKey().Key)
					{
						// Play again.
						case ConsoleKey.Y:
							StartRound();
							break;

						// Exit.
						case ConsoleKey.N:
							Environment.Exit(0);
							break;
					}
				}
			}
		}

		/// <summary>
		/// Returns a random tetromino.
		/// </summary>
		/// <returns></returns>
		private Tetromino GetRandomTetromino()
		{
			switch (new Random().Next(7))
			{
				case 0:
					return new ITetromino();
				case 1:
					return new OTetromino();
				case 2:
					return new TTetromino();
				case 3:
					return new STetromino();
				case 4:
					return new ZTetromino();
				case 5:
					return new JTetromino();
				default:
					return new LTetromino();
			}
		}

		/// <summary>
		/// Resets the hand to the starting position with a new tetromino.
		/// </summary>
		/// <param name="newHand">The given a value, this tetromino will be used to create the new hand. Otherwise a random will be chosen.</param>
		private void ResetHand(Tetromino newHand = null)
		{
			// Handle the picking of the tetromino to spawn as the hand.
			if (newHand is null)
			{
				hand = next.Clone();
				next = GetRandomTetromino();
			}
			else
				hand = newHand;

			// Set the hand's information.
			UpdateHandInfo();
			y = 0 - hand.GetHeightOffset();
			lastY = well.DropTetromino(hand, x);
			x = Well.WIDTH / 2 - hand.BoundingBoxWidth / 2;
			if (hand.BoundingBoxWidth == 3)
				x--;

			// If the hand is already colliding with something, then it's a game over.
			if (well.CollidesTetromino(hand, x, y))
				EndRound();
		}

		/// <summary>
		/// Updates the information about the hand's bounding box.
		/// </summary>
		private void UpdateHandInfo()
		{
			handWidth = hand.GetActualWidth();
			handHeigh = hand.GetActualHeight();
			offsetX = hand.GetWidthOffset();
		}

		/// <summary>
		/// Places the hand.
		/// </summary>
		/// <param name="y">Leave blank (-1) and the current y position will be used.</param>
		private void PlaceHand(int y = -1)
		{
			switched = false;

			// Place the hand.
			if (y == -1)
				y = this.y;
			well.PlaceTetromino(hand, x, y);

			// Check for full lines.
			HandleFullRows();

			// Reset the hand.
			ResetHand();
		}

		/// <summary>
		/// Checks if there are any full lines. If there are it removes them, and adds points to the score.
		/// </summary>
		private void HandleFullRows()
		{
			int count = well.CountAndRemoveFullRows();
			if (count > 0)
			{
				combo++;
				score += count * 2 - 1 + combo;
			}
			else
				combo = 0;
		}

		/// <summary>
		/// Checks for collisions and kicks the hand out of them if necessary.
		/// </summary>
		/// <param name="fallback">The bounding box to fall back to if no kick could be found.</param>
		private void HandleKicks(bool[,] fallback)
		{
			// No collision has taken place, return.
			if (!well.CollidesTetromino(hand, x, y))
				return;

			// Try to kick for all different collisions.
			else if (y < 0)
				y = 0;
			else if (x < Well.WIDTH && !well.CollidesTetromino(hand, x + 1, y))
				x += 1;
			else if (x > 0 && !well.CollidesTetromino(hand, x - 1, y))
				x -= 1;
			else if (x + 1 < Well.WIDTH && !well.CollidesTetromino(hand, x + 2, y))
				x += 2;
			else if (x - 1 > 0 && !well.CollidesTetromino(hand, x - 2, y))
				x -= 2;
			else if (!well.CollidesTetromino(hand, x, y - 1))
				y -= 1;
			else if (!well.CollidesTetromino(hand, x, y - 2))
				y -= 2;

			// No kick could be found, reset rotation.
			else
				hand.UpdateBoundingBox(fallback);
		}

		/// <summary>
		/// Starts the round.
		/// </summary>
		private void StartRound()
		{
			Console.Clear();
			well = new Well();

			dropSpan = INITIAL_DROP_SPAN;
			lastDrop = DateTime.Now;

			hold = null;
			next = GetRandomTetromino();
			ResetHand();

			score = 0;
			roundRunning = true;
		}

		/// <summary>
		/// Ends the round (game over).
		/// </summary>
		private void EndRound()
		{
			roundRunning = false;

			// Make the screen blink for a bit to signal a game end.
			string s = GetPrint();
			for (int i = 0; i < 10; i++)
			{
				Thread.Sleep(400);
				if (i % 2 == 0)
					Console.Clear();
				else
					Console.WriteLine(s);
			}

			// Write the end menu.
			Console.Clear();
			Console.WriteLine(GetEndPrint());
		}

		//// Key input actions.

		/// <summary>
		/// Moves the hand right, while handling for collisions.
		/// </summary>
		private void MoveRight()
		{
			if (x + handWidth + 1 < Well.WIDTH &&        // That the move won't result in it going out or the well.
				!well.CollidesTetromino(hand, x + 1, y)) // That the move won't result in a collision.
				x++;
		}

		/// <summary>
		/// Moves the hand left, while handling for collisions.
		/// </summary>
		private void MoveLeft()
		{
			if (x + offsetX > 0 &&                                 // That the move won't result in it going out or the well.
				!well.CollidesTetromino(hand, x - 1, y)) // That the move won't result in a collision.
				x--;
		}

		/// <summary>
		/// Rotates the hand clock wise, while handling for collisions.
		/// </summary>
		private void RotateRight()
		{
			Rotate(MathUtilities.RotateMatrixClockWise);
		}

		/// <summary>
		/// Rptates the hand counter clock wise, while handling for collisions.
		/// </summary>
		private void RotateLeft()
		{
			Rotate(MathUtilities.RotateMatrixCounterClockwise);
		}

		/// <summary>
		/// Handles the actual rotation, while handling for collisions.
		/// </summary>
		/// <param name="rotateFunction">The rotation function to use.</param>
		private void Rotate(Func<bool[,], bool[,]> rotateFunction)
		{
			bool[,] fallback = hand.BoundingBox;

			// Rotate the bounding box if it can.
			if (hand.CanRotate)
				hand.UpdateBoundingBox(rotateFunction(hand.BoundingBox));

			// Otherwise, if there's a rotated instance of the bounding box, toggle it.
			else if (hand.RotatedBoundingBox is bool[,])
				if (hand.BoundingBox == hand.RotatedBoundingBox) // Set it to the original bounding box ("rotate back").
					hand.UpdateBoundingBox(hand.InitialBoundingBox);
				else
					hand.UpdateBoundingBox(hand.RotatedBoundingBox); // Set it to the rotated bounding box ("rotate).

			// If this rotation has caused any collisions, make sure it's kicked properly.
			HandleKicks(fallback);

			// Update the information about the hand, as the rotation might have changed something.
			UpdateHandInfo();
		}

		/// <summary>
		/// Drops the hand a single cell, while handling for collisions.
		/// </summary>
		private void DropHandOnce()
		{
			// If the hand is in the lowest position it can be, place it there and reset the hand.
			if (y == lastY)
				PlaceHand();

			// Otherwise drop the hand as usual.
			else
				y++;

			// Set the last drop to now.
			lastDrop = DateTime.Now;
		}

		/// <summary>
		/// Drops the hand to the lowest it can currently go, while handling for collisions.
		/// </summary>
		private void DropHand()
		{
			PlaceHand(lastY);
		}

		/// <summary>
		/// Holds the current hand, and switches the hand to the previously held tetromino. If there is no such previous one, a new one will be spawned.
		/// </summary>
		private void HoldHand()
		{
			if (switched)
				return;

			// If there is no previous hold, simply spawn a new tetromino in the hand (and hold the current hand).
			if (hold is null)
			{
				// Clone the hand to the hold, with a reset bounding box (removes any rotations).
				hold = hand.Clone();

				// Reset the hand with a new tetromino.
				ResetHand();
			}

			// Otherwise, if there is a previous hold, simply switch the hand and hold.
			else
			{
				Tetromino temp = hand.Clone();

				// Reset the hand with the tetromino in the hold.
				ResetHand(hold);

				// Set the hold to the previous hand.
				hold = temp;
			}

			switched = true;
		}
	}
}
