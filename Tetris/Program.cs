﻿/// <summary>
/// This work has been dedicated to Our Lord, Jesus Christ.
/// 
///			||
///			||
///		[][][][][]
///			||
///			||
///			||
///			||
/// 
/// </summary>
namespace Tetris
{
	class Program
	{
		static void Main(string[] args)
		{

			new Game().Start();

		}
	}
}
