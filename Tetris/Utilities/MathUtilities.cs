﻿namespace Tetris.Utilities
{
	class MathUtilities
	{

		public static T[,] RotateMatrixCounterClockwise<T>(T[,] oldMatrix)
		{
			T[,] newMatrix = new T[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
			int newColumn, newRow = 0;
			for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
			{
				newColumn = 0;
				for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
				{
					newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
					newColumn++;
				}
				newRow++;
			}
			return newMatrix;
		}

		public static T[,] RotateMatrixClockWise<T>(T[,] oldMatrix)
		{
			return RotateMatrixCounterClockwise(RotateMatrixCounterClockwise(RotateMatrixCounterClockwise(oldMatrix)));
		}

	}
}
