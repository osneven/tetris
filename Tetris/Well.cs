﻿using System.Text;
using Tetris.Entity;

namespace Tetris
{
	/// <summary>
	/// Contains information about the playing board and all the tetrominos on it.
	/// </summary>
	class Well
	{
		public const int WIDTH = 10, HEIGHT = 20;

		public bool[,] Cells { get; private set; }

		public Well()
		{
			Cells = new bool[HEIGHT, WIDTH];
		}

		/// <summary>
		/// Gets the string to print, containing the well and its contained tetrominos to the screen.
		/// </summary>
		/// <param name="hand">The hand to print in the well.</param>
		/// <param name="x">The position of the hand on the x-axis.</param>
		/// <param name="y">The position of the hand on the y-axis.</param>
		/// <returns>The string to print to the screen.</returns>
		public string GetPrint(Tetromino hand, int x, int y)
		{
			// Get the row to print the ghost on.
			int ghostY = DropTetromino(hand, x, y);

			// Add all columns to the string.
			StringBuilder screen = new StringBuilder();
			for (int i = 0; i < HEIGHT; i++)
			{
				// Add the current row to the string.
				for (int j = 0; j < WIDTH; j++)
				{
					if (j == 0)
						screen.Append(" ");

					bool filled = false;
					int dx = j - x, dy = i - y;

					// Check if the current cell contains a filled cell of the hand.
					filled = Cells[i, j] || (hand.Contains(dx, dy) && hand.BoundingBox[dy, dx]);

					// Handle for the ghost.
					if (!filled)
					{
						int ghostDy = i - ghostY;
						if (i >= ghostY && hand.Contains(dx, ghostDy) && hand.BoundingBox[ghostDy, dx])
						{
							screen.Append($" +");
							continue;
						}
					}

					// Add the cell to the string.
					screen.Append($" {ToStringCell(filled)}");
				}

				// The current row has finished, at a line break.
				screen.Append("\n");
			}
			return screen.ToString();
		}

		/// <summary>
		/// If the cells is filled (the bool is true) a '#' is returned. Otherwise, if the cell is empty, '.' is returned.
		/// </summary>
		/// <param name="cell">The cell to check for.</param>
		/// <returns>The string representation of the cell.</returns>
		private string ToStringCell(bool cell)
		{
			return cell ? "#" : ".";
		}

		/// <summary>
		/// Places a tetromino in the well. The given position will represent the top-left corner of the tetromino's bounding box.
		/// </summary>
		/// <param name="tetromino">The tetromino to place.</param>
		/// <param name="x">The position on the x-axis of the well to place it in.</param>
		/// <param name="y">The position on the y-axis of the well to place it in.</param>
		public void PlaceTetromino(Tetromino tetromino, int x, int y)
		{
			// Place the tetromino in the well.
			for (int i = 0; i < tetromino.BoundingBoxHeight; i++)
				for (int j = 0; j < tetromino.BoundingBoxWidth; j++)
				{
					int dx = x + i, dy = y + j;
					if (dx >= 0 && dy >= 0 && WIDTH > dx && HEIGHT > dy)
						if (tetromino.BoundingBox[j, i])
							Cells[dy, dx] = true;
				}
		}

		/// <summary>
		/// Returns the lowest a tetromino can be placed given its position on the x-axis.
		/// </summary>
		/// <param name="tetromino">The tetromino to check for.</param>
		/// <param name="x">The position on the x-axis.</param>
		/// <param name="y">The postiton on the y-axis to check on or below.</param>
		/// <returns>The lowest row the tetromino can be placed in.</returns>
		public int DropTetromino(Tetromino tetromino, int x, int y = 0)
		{
			// Make sure y is below 0.
			if (y < 0)
				y = 0;

			int width = tetromino.GetActualWidth(), height = tetromino.GetActualHeight();
			int[][] filledCells = tetromino.GetFilledCells();

			// Go through all the rows.
			int final = HEIGHT - height;
			for (int i = y; i < final; i++)
			{
				// Go through all the positions of the cells filled in the tetromino
				foreach (int[] cell in filledCells)
				{
					// Check if the current filled cell position, in relation to the row and column itterated through,
					// is already filled in the well, a collision has occurred.
					int dx = cell[1] + x;
					if (dx >= 0 && dx < WIDTH && Cells[cell[0] + i, cell[1] + x])
					{
						// Return the previous row.
						return i - 1;
					}
				}
			}

			// No collision occured, simply return the lowest location for the tetromino to drop.
			return final - 1;
		}

		/// <summary>
		/// Determines whether or not a tetromino on the given position collides with a filled cell in the well.
		/// </summary>
		/// <param name="tetromino">The tetromino to check for.</param>
		/// <param name="x">The position on the x-axis.</param>
		/// <param name="y">The position on the y-axis.</param>
		/// <returns>Returns true if it collides, false if otherwise.</returns>
		public bool CollidesTetromino(Tetromino tetromino, int x, int y)
		{
			// Go through all the filled cells in the tetromino.
			int[][] filledCells = tetromino.GetFilledCells();
			foreach (int[] cell in filledCells)
			{
				// If the current cell, relative to the position given, is already filled in the well, a collision has been found.
				// Also return a positive if the tetromino is outside the well.
				int dx = cell[1] + x, dy = cell[0] + y;
				if (dx < 0 || dy < 0 || dx >= WIDTH || dy >= HEIGHT || Cells[dy, dx])
					return true;
			}

			// If we're here, no collisions were found.
			return false;
		}

		/// <summary>
		/// Counts and removes all the full rows in the well.
		/// </summary>
		/// <returns>The amount of full rows.</returns>
		public int CountAndRemoveFullRows()
		{
			// Go through all the rows.
			int counter = 0;
			for (int i = 0; i < HEIGHT; i++)
			{
				// Check if the current row is full.
				bool full = true;
				for (int j = 0; j < WIDTH; j++)
					if (!Cells[i, j])
					{
						full = false;
						break;
					}
				if (full)
				{
					// Move all the above rows down.
					for (int j = i - 1; j >= 0; j--)
						for (int k = 0; k < WIDTH; k++)
							Cells[j + 1, k] = Cells[j, k];

					// Count it.
					counter++;
				}
			}
			return counter;
		}
	}
}
